# Project Title

URL Shortener back-end

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes

### Prerequisites

* java 1.8

### Installing

Command `gradlew bootRun` will launch the server with values provided in `application.properties`. 
By default it run on port 8080 an connect to a mongodb database in localhost 27017.

## Running the tests

Command `gradlew test` will run automated tests. It will use an embedded mongodb.

## Deployment

Command `gradlew bootJar` will create a Spring Boot jar that can be found in `build/libs`

package fr.will.challengenotarius.rest.model;

import fr.will.challengenotarius.dao.documents.UrlShortened;

public class UrlShortenResponse {

    private String shortUrl;
    private String fullUrl;

    public String getShortUrl() {
        return shortUrl;
    }

    public UrlShortenResponse setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
        return this;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public UrlShortenResponse setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
        return this;
    }

    public static UrlShortenResponse from(UrlShortened urlShortened){
        return new UrlShortenResponse()
                .setShortUrl(urlShortened.getShortUrl())
                .setFullUrl(urlShortened.getFullUrl());
    }
}

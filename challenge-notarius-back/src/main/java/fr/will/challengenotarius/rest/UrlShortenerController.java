package fr.will.challengenotarius.rest;

import fr.will.challengenotarius.business.UrlShortenerService;
import fr.will.challengenotarius.rest.model.UrlShortenInput;
import fr.will.challengenotarius.rest.model.UrlShortenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UrlShortenerController {

    @Autowired
    private UrlShortenerService urlShortenerService;

    @PostMapping("/shorten")
    public UrlShortenResponse shorten(@RequestBody UrlShortenInput input){
        return urlShortenerService.shorten(input.getFullUrl());
    }

    @GetMapping("/unshorten")
    public UrlShortenResponse unshorten(@RequestParam(value = "url") String shortUrl){
        return urlShortenerService.unshorten(shortUrl);
}
}

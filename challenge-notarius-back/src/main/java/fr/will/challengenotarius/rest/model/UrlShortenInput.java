package fr.will.challengenotarius.rest.model;

public class UrlShortenInput {
    private String fullUrl;

    public String getFullUrl() {
        return fullUrl;
    }

    public UrlShortenInput setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
        return this;
    }
}

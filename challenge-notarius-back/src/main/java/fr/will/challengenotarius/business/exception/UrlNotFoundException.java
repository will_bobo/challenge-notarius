package fr.will.challengenotarius.business.exception;

public class UrlNotFoundException extends RuntimeException {

    public UrlNotFoundException() {
        super("Url not found");
    }
}

package fr.will.challengenotarius.business;

import fr.will.challengenotarius.business.exception.UrlNotFoundException;
import fr.will.challengenotarius.business.exception.UrlShortenerException;
import fr.will.challengenotarius.dao.documents.UrlShortened;
import fr.will.challengenotarius.dao.repositories.IUrlShortenedRepository;
import fr.will.challengenotarius.rest.model.UrlShortenResponse;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

@Component
public class UrlShortenerService {

    @Autowired
    private IUrlShortenedRepository urlShortenedRepository;
    @Value("${shortener.domain:http://www.urlshortener.ca/}")
    private String domain;

    public UrlShortenerService(IUrlShortenedRepository urlShortenedRepository) {
        this.urlShortenedRepository = urlShortenedRepository;
    }

    public UrlShortenResponse shorten(String fullUrl) {
        validateUrl(fullUrl);

        Optional<UrlShortened> optUrlShortened = urlShortenedRepository.findByFullUrl(fullUrl);
        if(optUrlShortened.isPresent()){ //if already existing url, return it
            return new UrlShortenResponse()
                    .setFullUrl(fullUrl)
                    .setShortUrl(optUrlShortened.get().getShortUrl());
        } else { //otherwise, return a new one
            String shortenedUrl;
            do{
                // or use https://commons.apache.org/proper/commons-text/javadocs/api-release/org/apache/commons/text/RandomStringGenerator.html ?
                String generatedId = RandomStringUtils.randomAlphanumeric(10);
                //todo add in properties
                shortenedUrl = domain+generatedId;
           } while(urlShortenedRepository.findByShortUrl(shortenedUrl).isPresent());
            urlShortenedRepository.save(new UrlShortened().setFullUrl(fullUrl).setShortUrl(shortenedUrl));
            return new UrlShortenResponse()
                    .setFullUrl(fullUrl)
                    .setShortUrl(shortenedUrl);
        }
    }

    public UrlShortenResponse unshorten(String shortUrl) {
        validateUrl(shortUrl);
        Optional<UrlShortened> optUrlShortened = urlShortenedRepository.findByShortUrl(shortUrl);
        if(optUrlShortened.isPresent()){
            return UrlShortenResponse.from(optUrlShortened.get());
        } else {
            throw new UrlNotFoundException();
        }
    }

    private void validateUrl(String fullUrl){
        try {
            // or use https://commons.apache.org/proper/commons-validator/apidocs/org/apache/commons/validator/routines/UrlValidator.html ?
            new URL(fullUrl);
        } catch (MalformedURLException e) {
            throw new UrlShortenerException("Url not valid",e);
        }
    }


}

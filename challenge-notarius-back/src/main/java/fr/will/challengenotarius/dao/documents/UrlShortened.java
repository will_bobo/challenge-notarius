package fr.will.challengenotarius.dao.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

public class UrlShortened {

    @Id
    private String id;

    @Indexed
    private String fullUrl;
    @Indexed
    private String shortUrl;

    public UrlShortened() {}

    public UrlShortened(String fullUrl, String shortUrl) {
        this.fullUrl = fullUrl;
        this.shortUrl = shortUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public UrlShortened setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
        return this;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public UrlShortened setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
        return this;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("UrlShortened{");
        sb.append("id='").append(id).append('\'');
        sb.append(", fullUrl='").append(fullUrl).append('\'');
        sb.append(", shortUrl='").append(shortUrl).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
package fr.will.challengenotarius.dao.repositories;

import fr.will.challengenotarius.dao.documents.UrlShortened;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface IUrlShortenedRepository extends MongoRepository<UrlShortened, String> {

    Optional<UrlShortened> findByFullUrl(String fullUrl);
    Optional<UrlShortened> findByShortUrl(String shortUrl);

}

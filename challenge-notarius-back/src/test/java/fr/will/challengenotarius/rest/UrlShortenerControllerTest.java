package fr.will.challengenotarius.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.will.challengenotarius.dao.documents.UrlShortened;
import fr.will.challengenotarius.dao.repositories.IUrlShortenedRepository;
import fr.will.challengenotarius.rest.model.UrlShortenInput;
import fr.will.challengenotarius.rest.model.UrlShortenResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class UrlShortenerControllerTest {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private IUrlShortenedRepository urlShortenedRepository;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void it_generate_a_shortcut_url() throws Exception {
        String fullUrl = "http://www.google.ca";

        UrlShortenInput input = new UrlShortenInput()
                .setFullUrl(fullUrl);

        this.mvc.perform(post("/shorten")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(input)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.fullUrl").value(fullUrl))
                .andExpect(jsonPath("$.shortUrl").isNotEmpty());

        assertThat(urlShortenedRepository.findAll()).hasSize(1);
        assertThat(urlShortenedRepository.findByFullUrl(fullUrl).get().getShortUrl()).isNotEmpty();
    }


    @Test
    public void it_return_a_fullUrl_from_a_short_url() throws Exception {
        String shortUrl = "http://www.urlshortenen.ca/1234567890";
        String fullUrl = "http://www.google.ca";
        urlShortenedRepository.save(new UrlShortened().setShortUrl(shortUrl).setFullUrl(fullUrl));

        this.mvc.perform(get("/unshorten")
                .param("url", shortUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.fullUrl").value(fullUrl))
                .andExpect(jsonPath("$.shortUrl").value(shortUrl));
    }

    @Test
    public void it_return_404_when_short_url_does_not_exist() throws Exception {
        String shortUrl = "http://www.urlshortenen.ca/1234567890";

        this.mvc.perform(get("/unshorten")
                .param("url", shortUrl))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status").value(404))
                .andExpect(jsonPath("$.message").isNotEmpty());

    }

    @Test
    public void two_same_full_url_return_same_short_url() throws Exception {
        String fullUrl = "http://www.google.ca";

        UrlShortenInput input = new UrlShortenInput()
                .setFullUrl(fullUrl);

        // generation of first short url
        String body = this.mvc.perform(post("/shorten")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(input)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.fullUrl").value(fullUrl))
                .andExpect(jsonPath("$.shortUrl").isNotEmpty())
                .andReturn().getResponse().getContentAsString();

        UrlShortenResponse response = objectMapper.readValue(body, UrlShortenResponse.class);

        // check that we receive the same short url
        this.mvc.perform(post("/shorten")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(input)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.shortUrl").value(response.getShortUrl()));
    }

}
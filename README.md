# Challenge Notarius

Simplest way to run this challenge is :
* To have a MongoDB running on 27017 wihout authentication
* Run `npm install` then `npm run start` in __challenge-notarius-front__ and access http://localhost:4200
* Run `gradlew bootRun` in __challenge-notarius-back__

See readmes in each folder for more detail.
# Project Title

URL Shortener front-end

## Getting Started

Before all, download dependencies with `npm install`

### Prerequisites

* node 8.x +
* npm 5.x +

### Installing

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

A proxy is configured from `/api` endpoint to `localhost:8080`

## Deployment

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. You will have to create proxy from __/api__ endpoint to the server. 

import { Component, Input, OnInit } from '@angular/core';
import { UrlShortenerService } from './services/url-shortener.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: []
})
export class AppComponent implements OnInit {

  urlInput: string;
  errorMessage = '';
  result: any;
  requestRunning: boolean;

  constructor(private urlShortenerService: UrlShortenerService) {

  }

  ngOnInit(): void {
    this.urlInput = 'http://www.google.ca';
  }

  onShortenClick() {
    this.errorMessage = '';
    this.result = undefined;
    this.requestRunning = true;
    this.urlShortenerService.shorten(this.urlInput)
      .subscribe(result => {
        this.requestRunning = false;
        this.result = result.shortUrl;
      },
        error => {
          this.dealWithError(error);
        });
  }

  onUnshortenClick() {
    this.errorMessage = '';
    this.result = undefined;
    this.requestRunning = true;
    this.urlShortenerService.unshorten(this.urlInput)
      .subscribe(result => {
        this.requestRunning = false;
        this.result = result.fullUrl;
      },
        error => {
          this.dealWithError(error);
        });
  }

  private dealWithError(error) {
    this.requestRunning = false;
    if (error.status === 400) {
      this.errorMessage = error.error.message;
    } else if (error.status === 404) {
      this.errorMessage = error.error.message;
    } else {
      this.errorMessage = 'An error occured';
      console.log(error);
    }
  }

}

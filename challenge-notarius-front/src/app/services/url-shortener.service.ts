import { environment } from './../../environments/environment';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { UrlShortened } from '../model/UrlShortened';

@Injectable({
    providedIn: 'root',
})
export class UrlShortenerService {
    constructor(private http: HttpClient) {
    }

    shorten(fullUrl: string): Observable<UrlShortened> {
        const body = { fullUrl: fullUrl };
        return this.http
            .post<UrlShortened>(environment.apiUrl + 'shorten', body);
    }

    unshorten(shortUrl: string): Observable<UrlShortened> {
        const listParams = new HttpParams()
            .set('url', shortUrl);
        return this.http
            .get<UrlShortened>(environment.apiUrl + 'unshorten', {
                params: listParams
            });
    }
}

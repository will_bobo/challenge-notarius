export interface UrlShortened {
    fullUrl: string;
    shortUrl: string;
}
